#ifndef _SQUEUE_DYNAMIC_H_
#define _SQUEUE_DYNAMIC_H_

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/semaphore.h>

#define CLASS_NAME "squeue"
#define MAX_MESSAGE 10
#define MSG_SIZE 80
#define SQUEUE_DEVICES 4 

static struct class *squeue_class = NULL;
static dev_t squeue_major_number;
static dev_t squeue_minor_number = 0;

struct queue_head {
	struct queue_head *next;
	struct queue_head *prev;
};

typedef struct queue_head QUEUE;

struct message {
	int messageID;
	int src_id;
	int rcv_id;
	uint64_t acc_time;
	uint64_t enq_time;
	char message[MSG_SIZE];
};

typedef struct message MESSAGE;

struct queue_element {
	MESSAGE *msg;
	struct queue_element *next;
	struct queue_element *prev;
};

typedef struct queue_element ELEMENT;

struct squeue_dev {
	int queue_size;
	char name[20];
	dev_t devno;
	struct cdev cdev;			/* The cdev structure */
	struct device *dev;			/* The deivce structure */
	struct semaphore sem;
	ELEMENT *head;				/* Queue head */
};
typedef struct squeue_dev SQUEUE;
SQUEUE *squeue_devp;

int squeue_open(struct inode *inode, struct file *file);
int squeue_release(struct inode *inode, struct file *file);
ssize_t queue_write(struct file *file, const char *buf,
		size_t count, loff_t *ppos);
ssize_t queue_read(struct file *file, char *buf,
		size_t count, loff_t *ppos);
void squeue_enqueue(char *element,
		struct queue_head *prev, struct queue_head *head, int msg_ID);
struct queue_element* squeue_dequeue(struct queue_head *message);
uint64_t rdtsc(void);

int __init queue_init(void);
void __exit queue_exit(void);

#endif
