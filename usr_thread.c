#include<stdio.h>
#include<pthread.h>
#include<fcntl.h>
#include<stdint.h>
#include<string.h>
#include<time.h>
#include<stdlib.h>
#include<semaphore.h>
#include<inttypes.h>

/***********************************************
 * 3 Sender Threads
 * 3 Receiver Threads
 * Max user message size 80
 **********************************************/
#define SND_THREADS 3
#define RCV_THREADS 3
#define MSG_SIZE 80

struct message{
	int messageID;
	int src_id;
	int rcv_id;
	uint64_t acc_time;	
	uint64_t enq_time;
	char message[MSG_SIZE];
};

int msg = 0;
sem_t sem;

/**********************************************
 * Function: insert into queue user message
 *
 * Param:  threadid -- to enumerate threads
 * *******************************************/
void *insert(void *threadid){
	int fd;
	int ret;
	int tid = *((int*)threadid);
	time_t start;
	time_t end;
	
	struct message send_message;
	int i = 0;
	fd = open("/dev/bus_in_q", O_RDWR);
	if(fd < 0){
		printf("\nCannot open device");
		pthread_exit(NULL);
	}
	else{
		start = time(NULL);
		end = time(NULL);
		while(end - start < 10){
			sem_wait(&sem);

			send_message.messageID = msg++;
	
			sem_post(&sem);
			send_message.src_id = tid;
			send_message.rcv_id = rand() % 3;
			send_message.acc_time = 0;
			send_message.enq_time = 0;
			sprintf(send_message.message, "Hello from sender %d", tid);
			ret = write(fd, &send_message, sizeof(send_message));
			if(ret == 0){
				printf("\nError in sending message.Sleeping");
				usleep((rand() % 10 + 1) * 1000);
			}
			else{
				usleep((rand() % 10 + 1) * 1000);
			}
			end = time(NULL);
		}
	}
	close(fd);
	pthread_exit(NULL);
}

/****************************************************************
 * Function: remove user message from queue
 *
 * Param: threadid -- to enumerate threads
 *
 ***************************************************************/
void *remove_msg(void *threadid){
	int fd;
	int ret;
	time_t end;
	time_t start;
	int tid = *((int*)threadid);
	struct message rcv_message;

	switch(tid){
		case 0: fd = open("/dev/bus_out_q1", O_RDWR);
			if(fd < 0){
				printf("\nCannot open device");
				exit(-1);
			}
			break;
		case 1: fd = open("/dev/bus_out_q2", O_RDWR);
			if(fd < 0){
				printf("\nCannot open device");
				exit(-1);
			}
			break;
		case 2: fd = open("/dev/bus_out_q3", O_RDWR);
			if(fd < 0){
				printf("\nCannot open device");
				exit(-1);
			}
	}

	start = time(NULL);
	end = time(NULL);	
	while(end - start < 11){
		ret = read(fd, &rcv_message, sizeof(struct message));
		if(ret != 0){
			usleep((rand() % 10 + 1) * 1000);
		}
		else{	
			printf("\nReceiver %d -- Message: %s", tid, rcv_message.message);
			printf("\nReceiver %d -- Accumulated Queueing time: %f", tid, (double) rcv_message.acc_time / 400000000);
			usleep((rand() % 10 + 1) * 1000);
		}
		end = time(NULL);
	}
	close(fd);
	pthread_exit(NULL);
}

/********************************************************
 * Function: bus daemon
 *
 * Param: threadid
 *
 * Notes: bus daemon thread. Dequeues message from input
 *        queue and based on receiver id puts into
 *	  receiver queue
 *
 *******************************************************/
void *bus(void *threadid){
	int fi, fr1, fr2, fr3;
	int ret;
	time_t end;
	time_t start;
	
	struct message rcv_message;
	fi = open("/dev/bus_in_q", O_RDWR);
	if(fi < 0){
		printf("\nCannot open send queue");
		exit(-1);
	}
	fr1 = open("/dev/bus_out_q1", O_RDWR);
	if(fr1 < 0){
		printf("\nCannot open receiver queue 1");
		exit(-1);
	}
	
	fr2 = open("/dev/bus_out_q2", O_RDWR);
	if(fr2 < 0){
		printf("\nCannot open receiver queue 2");
		exit(-1);
	}
		
	fr3 = open("/dev/bus_out_q3", O_RDWR);
	if(fr3 < 0){
		printf("\nCannot open receiver queue 3");
		exit(-1);
	}

	start = time(NULL);
	end = time(NULL);
	while(end - start < 11){
		ret = read(fi, &rcv_message, sizeof(struct message));
		if(ret != 0){
			usleep((rand() % 10 + 1) * 1000);
		}
		else{
			switch(rcv_message.rcv_id){							/* Receiver id to switch output queue */
				case 0:	ret = write(fr1, &rcv_message, sizeof(struct message));
					if(ret == 0){
						printf("\nBus Error: Cannot send to queue");
						usleep((rand() % 10 + 1) * 1000);
					}
					else{
						usleep((rand() % 10 + 1) * 1000);
					}
					break;
				
				case 1:	ret = write(fr2, &rcv_message, sizeof(struct message));
					if(ret == 0){
						printf("\nBus Error: Cannot send to queue");
						usleep((rand() % 10 + 1) * 1000);
					}
					else{
						usleep((rand() % 10 + 1) * 1000);
					}
					break;
				
				case 2:	ret = write(fr3, &rcv_message, sizeof(struct message));
					if(ret == 0){
						printf("\nBus Error: Cannot send to queue");
						usleep((rand() % 10 + 1) * 1000);
					}
					else{
						usleep((rand() % 10 + 1) * 1000);
					}
					break;
				default: printf("\nInvalid receiver id");
			}
		}
		end = time(NULL);
	}
	close(fi);
	close(fr1);
	close(fr2);
	close(fr3);
	pthread_exit(NULL);
}

/*************************************************************************************
 * Function: main
 *
 * Notes: Creates the 3 sender, 1 bus and 3 receiver threads
 *        Waits for threads to join before exiting
 * 					
 ************************************************************************************/	
int main(int argc, char **argv){
	pthread_t usr_thread[SND_THREADS];
	pthread_t bd_thread;
	pthread_t rcv_thread[RCV_THREADS];
	int ret;
	int i, k;

	int snd_thread_id[3];
	int rcv_thread_id[3];
	sem_init(&sem, 0, 1);
	for(i = 0; i < SND_THREADS; i++){
		snd_thread_id[i] = i;
		ret = pthread_create(&usr_thread[i], NULL, insert, &snd_thread_id[i]);			/* 3 Sender threads */
		if(ret){
			printf("\nERROR creating snd thread: %d", ret);
			return -1;
		}
	}
	ret = pthread_create(&bd_thread, NULL, bus, NULL);						/* 1 Bus daemon thread */
	if(ret){
		printf("\nERROR creating bus thread: %d", ret);
		return -1;
	}

	for(k = 0; k < RCV_THREADS; k++){
		rcv_thread_id[k] = k;
		ret = pthread_create(&rcv_thread[k], NULL, remove_msg, &rcv_thread_id[k]);
		if(ret){
			printf("\nERROR creating rcv threads: %d", ret);
			return -1;
		}
	}
	for(i = 0; i < SND_THREADS; i++){								/* Wait for all thread before exit */
		ret = pthread_join(usr_thread[i], NULL);
	}
	ret = pthread_join(bd_thread, NULL);
	for(i = 0; i < RCV_THREADS; i++){
		ret = pthread_join(rcv_thread[i], NULL);
	}
	printf("\nEXITING");
	return 0;
}
	
	
