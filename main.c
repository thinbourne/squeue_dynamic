#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
/*#include <sys/time.h>
#include <linux/ioctl.h>
#include <linux/rtc.h>*/
#include <time.h>
#include <stdlib.h>

#define MSG_SIZE 80
struct message{
	int messageID;
	int src_id;
	int rcv_id;
	uint64_t acc_time;
	uint64_t enq_time;
	char message[MSG_SIZE];
};

int main(int argc, char **argv)
{
	int fd, res;
	int fd1;
	struct message usr;
	int i = 0;
	char buff[1024];
	struct message send_message;
	send_message.messageID = 0;
	send_message.src_id = 0;
	send_message.rcv_id = 0;
	send_message.acc_time = 0;
	send_message.enq_time = 0;
	strcpy(send_message.message, "HELLO FROM SENDER");

	if(argc == 1){
		printf("\nError in number of arguments");
		return 0;
	}

	fd = open("/dev/bus_in_q", O_RDWR);
	fd1 = open("/dev/bus_out_q", O_RDWR);
	if (fd < 0 ){
		printf("Can not open device file.\n");		
		return 0;
	}else{
		if(strcmp("show", argv[1]) == 0){
			res = read(fd1, (char*)&usr, sizeof(struct message));
			printf("\nMESSAGE FROM DRIVER: %s", usr.message);
			printf("\nMESSAGE QUEUE TIME: %ld", usr.acc_time);
		}else if(strcmp("write", argv[1]) == 0){
			/*if(argc >= 3){
				sprintf(buff,"%s", argv[2]);
				for(i = 3; i < argc; i++)
					sprintf(buff,"%s %s",buff,argv[i]);
			}
			strncpy(send_message.message, buff, strlen(buff)+1);*/
			printf("\nSending message to queue");
			res = write(fd,&send_message, sizeof(send_message));
			if(res == sizeof(send_message)){
				printf("\nSuccess in sending to queue");
			}
		}
		/* close devices */
		close(fd);
	}
	return 0;
}
