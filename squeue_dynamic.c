/*****************************************************************************************************************
 * 
 * squeue.c --- Dynamic Squeue driver 
 * Copyright (C) 2014 Janakarajan Natarajan
 * 
 ****************************************************************************************************************/

#include "squeue_dynamic.h"

/*************************************************************************
 * Function: Read Time Stamp Counter (TSC)
 *
 * Return: 64-bit timestamp value
 *
 ************************************************************************/
uint64_t inline rdtsc(){
	unsigned int low, high;
	asm volatile("rdtsc"
			: "=a" (low), "=d" (high));
	return (uint64_t)high << 32 | low;
}

/************************************************************************
 * Function: Open squeue driver
 * 
 * Return: 0
 ************************************************************************/
int squeue_driver_open(struct inode *inode, struct file *file)
{
	SQUEUE *squeue_devp;
	squeue_devp = container_of(inode->i_cdev, SQUEUE, cdev);

	file->private_data = squeue_devp;
	printk("%s is openning \n", squeue_devp->name);
	return 0;
}

/************************************************************************
 * Function: Release squeue driver
 *
 * Return: 0
 ***********************************************************************/
int squeue_driver_release(struct inode *inode, struct file *file)
{
	SQUEUE *squeue_devp = file->private_data;
	printk("%s is closing\n", squeue_devp->name);
	
	return 0;
}

/************************************************************************
 * Function: Write to squeue driver
 * Adds the incoming user message to queue
 * 
 * Return: -ENOMEM
 *	   -EINVAL if unable to get semaphore lock 
 ***********************************************************************/
ssize_t squeue_driver_write(struct file *file, const char *buf,
           size_t count, loff_t *ppos)
{
	SQUEUE *squeue_devp = file->private_data;
	ELEMENT *element;
	uint64_t e_time;
	int copied;
	if(down_interruptible(&squeue_devp->sem)){
		printk("Could not get write sem\n");
		return -EINVAL;
	}

	if(squeue_devp->queue_size == 10){
		printk("%s QUEUE LIMIT REACHED\n", squeue_devp->name);
		up(&squeue_devp->sem);
		return -ENOMEM;
	}
	element = kmalloc(sizeof(ELEMENT), GFP_KERNEL);
	element->msg = kmalloc(sizeof(MESSAGE), GFP_KERNEL);
	if(!element){
		printk("Cannot assign\n");
		up(&squeue_devp->sem);
		return -ENOMEM;
	}
	if(!element->msg){
		printk("Cannot assign message memory\n");
		up(&squeue_devp->sem);
		return -ENOMEM;
	}
	copied = copy_from_user(element->msg, buf, count);
	if(copied == 0){
		printk("Successfully copied\n");
		printk("USER MESSAGE: %s\n", element->msg->message);
	}
	else{
		printk("Error in copying\n");
		up(&squeue_devp->sem);
		return -ENOMEM;
	}
	/**********************************************	
	 * Add user message to tail of queue.
	 * 
 	 * Change pointers to reflect message addition
	 **********************************************/
	squeue_devp->head->prev->next = element;
	element->next = squeue_devp->head;
	element->prev = squeue_devp->head->prev;
	squeue_devp->head->prev = element;
	e_time = rdtsc();
	element->msg->enq_time = e_time;
	squeue_devp->queue_size += 1;
	printk("%s Queue size: %d\n", squeue_devp->name, squeue_devp->queue_size);
	up(&squeue_devp->sem);
	return count;
}
/*****************************************************************************
 * Function: Read to squeue driver
 * Remove message from queue and send to user
 *
 * Return:  -ENOMEM
 *	    -EINVAL if unable to get semaphore lock
 ****************************************************************************/
ssize_t squeue_driver_read(struct file *file, char *buf,
           size_t count, loff_t *ppos)
{
	SQUEUE *squeue_devp = file->private_data;
	ELEMENT *element;
	uint64_t d_time;
	int bytes_read = 0;

	if(down_interruptible(&squeue_devp->sem)){
		printk("Could not get read sem\n");
		return -1;
	}

	if(squeue_devp->queue_size == 0){
		printk("%s QUEUE EMPTY\n", squeue_devp->name);
		up(&squeue_devp->sem);
		return -ENOMEM;
	}

	/*********************************************************************
 	 * Always remove front of linked list
	 *
	 ********************************************************************/
	element = squeue_devp->head->next;
	if(!element){
		printk("Error in queue\n");
		up(&squeue_devp->sem);
		return -ENOMEM;
	}

	d_time = rdtsc();
	element->msg->acc_time += d_time - element->msg->enq_time;

	/********************************************************************
 	 * Remove user message from queue
	 * 
	 * Message is removed from the front of the list.
	 * Change pointers to reflect message removal.
	 *******************************************************************/	
	bytes_read = copy_to_user(buf, element->msg, sizeof(MESSAGE));
	if(bytes_read == 0){
		printk("%s DEQUEUED: %s\n", squeue_devp->name, element->msg->message);
		squeue_devp->head->next = element->next;
		element->next->prev 	= element->prev;
		element->next  		= NULL;
		element->prev		= NULL;
	}
	else{
		printk("Not fully read\n");
		up(&squeue_devp->sem);
		return -1;
	}
	squeue_devp->queue_size -= 1;
	printk("%s QUEUE SIZE: %d\n", squeue_devp->name, squeue_devp->queue_size);
	kfree(element->msg);
	kfree(element);
	up(&squeue_devp->sem);
	return bytes_read;
}

/***************************************************************************
 * File operations structure 
 *
 ***************************************************************************/
static struct file_operations squeue_fops = {
    .owner		= THIS_MODULE,               /* Owner          */
    .open		= squeue_driver_open,        /* Open method    */
    .release	        = squeue_driver_release,     /* Release method */
    .write		= squeue_driver_write,       /* Write method   */
    .read		= squeue_driver_read,        /* Read method    */
};

/****************************************************************************
 * Function: Driver Initialization
 *
 * Initialize linked list head to be used in enqueue and dequeue operations
 *
 * Return: -1       Error
 * 	   -ENOMEM  Memory error
 *	    0       Success
 ***************************************************************************/
int __init squeue_driver_init(void){
	int i;
	int error;
	int device_num;

	/* Request dynamic allocation of a device major number */
	if (alloc_chrdev_region(&squeue_major_number, 0, SQUEUE_DEVICES, CLASS_NAME) < 0) {
		printk(KERN_DEBUG "Can't register device\n"); 
		return -1;
	}

	squeue_class = class_create(THIS_MODULE, CLASS_NAME);
	squeue_devp = (SQUEUE*)kmalloc(sizeof(SQUEUE) * SQUEUE_DEVICES, GFP_KERNEL);
	if (!squeue_devp) {
		printk("Bad Kmalloc\n"); return -ENOMEM;
		return -ENOMEM;
	}

	/***********************************************************************************
	 * Initialize cdev structure for 4 devices and setup linked list head for each
	 *
	 **********************************************************************************/	
	for(i = 0; i < SQUEUE_DEVICES; i++){
		device_num = MKDEV(MAJOR(squeue_major_number), squeue_minor_number + i);
		cdev_init(&squeue_devp[i].cdev, &squeue_fops);
		squeue_devp[i].cdev.owner 	= THIS_MODULE;
		squeue_devp[i].devno 		= device_num;
		error = cdev_add(&squeue_devp[i].cdev, device_num, 1);
		if(error){
			printk(KERN_NOTICE "Error %d adding squeue %d", error, i);
			return -1;
		}

		if(i == 0){
			sprintf(squeue_devp[i].name, "bus_in_q");
			squeue_devp[i].dev 	= device_create(squeue_class, NULL, device_num, NULL, "bus_in_q");
		}
		else{
			sprintf(squeue_devp[i].name, "bus_out_q%d", i);
			squeue_devp[i].dev 	= device_create(squeue_class, NULL, device_num, NULL, "bus_out_q%d", i);
		}
		
		/****************************************************************************
		 * Initially linked list head points to itself
		 *
		 ***************************************************************************/
		squeue_devp[i].head = kmalloc(sizeof(ELEMENT), GFP_KERNEL);
		squeue_devp[i].head->next 	= squeue_devp[i].head;
		squeue_devp[i].head->prev 	= squeue_devp[i].head;
		squeue_devp[i].queue_size 	= 0;
		sema_init(&squeue_devp[i].sem, 1);
	}
	return 0;
}

/********************************************************************************************
 * Function: Driver Exit 
 *
 * Cleanup devices and free allocated memory
 *******************************************************************************************/
void __exit squeue_driver_exit(void)
{
	// device_remove_file(squeue_dev_device, &dev_attr_xxx);
	/* Release the major number */
	int i;
	for(i = 0; i < SQUEUE_DEVICES; i++){
		cdev_del(&squeue_devp[i].cdev);
		device_destroy(squeue_class, squeue_devp[i].devno);
	}
	unregister_chrdev_region((squeue_major_number), SQUEUE_DEVICES);
	kfree(squeue_devp);
	
	/* Destroy driver_class */
	class_destroy(squeue_class);

	printk("SQUEUE driver removed.\n");
}

module_init(squeue_driver_init);
module_exit(squeue_driver_exit);
MODULE_LICENSE("GPL v2");
